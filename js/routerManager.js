var routerManager = Backbone.Router.extend({
	application: null,
	routes: {
		"" : "login",
		"login": "login",
		"home": "home",
		"profile": "perfil",
		"creategame": "createGame",
		"logout": "logout",
	},

	login: function(){
		if(this.checkSession()){
			Backbone.history.navigate("home", true);
			return;
		}else{
			this.application.session.deleteSession();
		}
		document.cookie =  'PHPSESSID=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		$('div.login').css("display", "block");
		$('div.home').css("display", "none");
	},

	home: function(){
		if(!this.checkSession()){
			this.login();
			return;
		}
		this.defaultValues();
		var span = $("span.location-span").html("Inicio");
		span.css("margin-left", "-"+span.width()/2)
		this.application.createHomePanels();
	},

	perfil: function(){
		if(!this.checkSession()){
			this.login();
			return;
		}
		this.defaultValues();
		var span = $("span.location-span").html("Perfil");
		span.css("margin-left", "-"+span.width()/2);
		this.application.createPerfilPanels();
	},

	createGame: function(){
		if(!this.checkSession()){
			this.login();
			return;
		}
		this.defaultValues();
		var span = $("span.location-span").html("Nueva Partida");
		span.css("margin-left", "-"+span.width()/2);
		this.application.createNewGame();
	},

	logout: function(){
		this.application.session.deleteSession();
		this.login();
	},

	defaultValues: function(){
		if(this.application.session.verificateChangePassword()){
			this.application.changePasswordModel();
		}
		// $.ajax(config)
		$('div.login').css("display", "none");
		$('div.home').css("display", "block");
		$("header[data-id=mainHeader]").css("display", "block");
		$("header[data-id=questionHeader]").css("display", "none");
		$("div.navbar").css("display", "block");
		
	},

	checkSession: function(){
		return this.application.session.isCreated();
	}

	
});