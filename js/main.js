var qestionApp;
$(document).ready(function(){
	$("button","header").click(function(){
		$( "#menu-panel" ).panel( "open" );
		$( "#menu-panel" ).empty();
		var list = new List({
			container: $("div.menu-panel")
		});
		
	})
	try{
		qestionApp = new Application();
	}catch(e){
		console.error(e);
	}
// qestionApp.session.deleteSession();
})
var Application = function(){
	if(typeof this.constructor == 'function'){
		this.constructor.apply(this, arguments);
	}
}
Application.prototype = {
	session: null,
	router: null,

	server: "http://192.168.1.55/qestionme/web/app_dev.php",
	constructor: function(){
		var me = this;
		this.session = new Session();
		this.router = new routerManager();
		this.router.application = this;
		setTimeout(function(){
			Backbone.history.start();
		}, 10);
	},
	createHomePanels: function(){
		var me = this;
		$("div.body-container").empty();
		var user = me.session.getUser();
		var divImg = $('<div></div>').addClass('div-img');
		var img = $("<img/>").attr("src", qestionApp.server+"/image/"+user.photo+"?width=130&height=130").addClass("img-user");
		divImg.append(img);
		img.click(function(){
			window.location = "#profile";
		})
		var button = $("<button></button>").addClass("btn btn-success btn-lg").attr("type", "button").html("Nueva Partida").css({
			width: "60%",
			marginLeft: "20%",
			marginTop: "40px"
		});
		button.click(function(){
			window.location = "#creategame";
		});
		$("div.body-container").append(divImg).append(button);
	},

	createNewGame: function(){
		$("div.body-container").empty();
		var createGame = new CreateGame({
			container: $("div.body-container")
		})
	},

	createPerfilPanels: function(){
		$("div.body-container").empty();
		var perfil = new Perfil({
			container: $("div.body-container")
		})
	},

	disableHeaders: function(){
		// $("header.header").css("display", "none");
		$("div.navbar").css("display", "none");
	},
	getHeaders: function(){
		return {
			'x-qestion': '/SessionToken SessionID="'+this.session.getToken()+'", Username="'+this.session.getUser().email+'"'
		};
	},
	reportError: function(title, content){
		var data = {
			title: title,
			content: content,
		};
		var config = ({
			type: "POST",
		  	headers: this.getHeaders(),
			url: qestionApp.server+"/home/error",
			data: JSON.stringify(data),
			contentType: 'application/json',
            dataType: "json",
		})
		$.ajax(config);
	},
}