var CreateGame = Backbone.View.extend({
	tag: "<div>",
	container: null,
	baseCls: "create-game",
	panelTable: null,
	players: [],
	constructor : function(config) {
		var self = this;
		self._ensureElement();
		config = config || {};
		_.extend(self, config);
		self.$el = $(self.tag);
		self.$el.addClass(self.baseCls);
		self.initialize();
	},
	initialize: function(){
		var me = this;
		me.selectPlayerType();
		me.render();
	},
	render: function(){
		this.container.append(this.$el);
	},

	createGame: function()
	{
		var me = this;
		var form = $("<form></form>").addClass('form-newgame');
		me.putPlayer(form);

		var label = $("<label></label>").html("Añade un nombre al juego").attr('for', 'inputName').addClass('lbl-name');
		var input = $("<input></input>").attr("type", "input").addClass("form-control").attr({
			"id": "inputName",
		}).css('text-align', 'center');
		var div = $("<div></div>").addClass("form-group div-newgame");
		var divButton = $("<div></div>").addClass("form-group");
		var button = $("<button></button>").html("Siguiente").addClass("btn btn-success btn-next").attr("type", "button");
		divButton.append(button);
		div.append(label).append(input);
		form.append(div).append(divButton);
		me.$el.append(form);

		button.click(function(){
			var span = $("span.location-span").html(input.val());
			me.selectPlayerType();
		});
	},

	selectPlayerType: function()
	{
		var me = this;
		me.$el.empty();
		var form = $("<form></form>");
		me.putPlayer(form);
		var titleDiv = $("<div></div>").addClass("form-group div-title-typegame");
		var title = $("<h2></h2>").html("Selecciona tipo de partida").addClass("title");
		titleDiv.append(title);
		form.append(titleDiv);
		var buttonsDiv = $("<div></div>").addClass("form-group").css("text-align", "center");
		var divRandom = $("<div></div>").addClass("div-typegame");
		var buttonRandom = $("<button></button>").addClass("btn btn-default btn-selecttypegame").attr({
			"aria-label": "Left Align",
			"type": "button",
			"title": "Inicia un juego aleatorio con 3 personas"
		});
		var spanRandom = $("<span></span>").addClass("glyphicon glyphicon-random span-selecttypegame").attr("aria-hidden", "true");
		var textRandom = $("<span></span>").html("Juego Aleatorio").addClass("span-typegame");
		buttonRandom.append(spanRandom);
		var divButtonRandom = $("<div></div>").append(buttonRandom);
		var divTextRandom = $("<div></div>").append(textRandom);
		divRandom.append(divButtonRandom).append(divTextRandom);

		var divSelect = $("<div></div>").addClass("div-typegame").css("margin-left", "20%");
		var buttonSelect = $("<button></button>").addClass("btn btn-default btn-selecttypegame").attr({
			"aria-label": "Left Align",
			"type": "button",
			"title": "Inicia un juego personalizado agregando tu a los participantes"
		});
		// var spanSelect = $("<span></span>").addClass("glyphicon glyphicon-play-circle span-selecttypegame").attr("aria-hidden", "true");
		var imgSelect = $("<img/>").attr("src", "images/friends.png");
		buttonSelect.append(imgSelect);
		var textSelect = $("<span></span>").html("Jugar con amigos").addClass("span-typegame");
		var divButtonSelect = $("<div></div>").append(buttonSelect);
		var divTextSelect = $("<div></div>").append(textSelect);
		divSelect.append(divButtonSelect).append(divTextSelect);
		form.append(buttonsDiv.append(divRandom).append(divSelect));
		me.$el.append(form);

		buttonRandom.click(function(){
			me.selectPlayersRandom();
		});
		// buttonSelect.click(function(){
		// 	me.selectPlayers();
		// })
	},

	selectPlayersRandom: function(){
		var me = this;
		var header = qestionApp.getHeaders();
		$.ajax({
		  	type: "GET",
		  	headers: header,
			url: qestionApp.server+"/qestion/main/random/player",
			success: function(data){
				if(data.total > 0)
				{
					var span = $("span.location-span").html(data.data.name);
					me.initializeDuel(data.data, data.start);
				}
				else{
					me.responseAnswer(true, 'Ya finalisaste este duelo');
				}

			},
			error: function(xhr, status, error) {
		    	try{
			    	var obj = jQuery.parseJSON(xhr.responseText);
			    	var n = noty({
			    		text: obj.message,
			    		timeout: 1000,
			    		type: "error"
			    	});
		    	}catch(ex){
		    		var n = noty({
			    		text: "Error",
			    		timeout: 1000,
			    		type: "error"
			    	});
		    	}
	    	}
	    });
	},

	selectPlayers: function()
	{
		var me = this;
		me.$el.empty();
		var form = $("<form></form>");
		// me.putPlayer(form);
		// var divCombo = $("<div></div>").addClass("form-group div-newgame");
		var combo = $("<div></div>").addClass('selectize-input items not-full has-options has-items').attr("value");

		form.append(combo);

		// var table = $("<table></table>").addClass("table table-players");
		// var caption = $("<caption></caption>").html("Jugadores Seleccionados");
		// var thead = $("<thead></thead>");
		// var tr = $("<tr></tr>");
		// var thEmail = $("<th></th>").html("Email").addClass("th1");
		// var thDelete = $("<th></th>").html("Eliminar");
		// table.append(caption).append(thead.append(tr.append(thEmail).append(thDelete)));

		// var divTable = $("<div></div>").addClass("form-group");
		// divTable.append(table);
		// form.append(divTable);
		me.$el.append(form);
		var user = qestionApp.session.getUser();
		// this.addPlayerToTable(user, table);

		var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' +
                  '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';

		var formatName = function(item) {
		    return $.trim((item.first_name || '') + ' ' + (item.last_name || ''));
		};

		combo.selectize({
		    plugins: ['remove_button'],
		    persist: false,
		    create: true,
		    render: {
		        item: function(data, escape) {
		            return '<div>"' + escape(data.text) + '"</div>';
		        }
		    },
		    onDelete: function(values) {
		        return confirm(values.length > 1 ? 'Are you sure you want to remove these ' + values.length + ' items?' : 'Are you sure you want to remove "' + values[0] + '"?');
		    }
		});
		// var config = {
		// 	headers: qestionApp.getHeaders(),
		// 	url: qestionApp.server+"/qestion/main/user",
	 //    	dataType: 'json',
	 //    	delay: 250,
	 //    	success: function(data){
	 //    		$.each(data.data, function(key, value){
	 //    			var op = $("<option></option>").attr({
	 //    				"value": value.email
	 //    			})
	 //    			combo.append(op);
	 //    		});
	 //    	},
		// }
		// $.ajax(config);


	},

	initializeDuel: function(duel, start)
	{
		$("div.body-container").empty();
		var gameView = new GameView({
			container: $("div.body-container"),
			game: duel,
			start: start
		});
	},

	addPlayerToTable: function(user, table)
	{
		var me = this;
		var tbody = $("<tbody></tbody>");
		var tr = $("<tr></tr>");
		var tdEmail = $("<td></td>").html(user.email);
		var btnDelete = $("<button></button>").attr({
			"type": "button",
			"aria-label": "Center Align"
		}).addClass("btn btn-default");
		var span = $("<span></span>").addClass("glyphicon glyphicon-remove").attr("aria-hidden", "true");
		btnDelete.append(span);
		var tdDelete = $("<td></td>").append(btnDelete);
		tbody.append(tr.append(tdEmail).append(btnDelete));
		table.append(tbody);
		me.players.push(user);
		btnDelete.click(function(){
			tbody.remove();
			console.log("antes");
			console.log(me.players);
			delete me.players[user];
			console.log("despues");
			console.log(me.players);
		});
	},

	putPlayer: function(form)
	{
		var me = this;
		var user = qestionApp.session.getUser();
		var divImg = $("<div></div>").addClass("form-group").css("text-align", "center");
		var imgGirl = $("<img/>").attr("src", 'images/chica.png').addClass("img-game");
		var imgBoy = $("<img/>").attr("src", 'images/chico.png').addClass("img-game");
		if(user.gender == "M")
		{
			divImg.append(imgBoy);
		}
		else if(user.gender == "F")
		{
			divImg.append(imgGirl);
		}
		else{
			divImg.append(imgBoy);
		}
		form.append(divImg);
	}
});
