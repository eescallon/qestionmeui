var GameView = Backbone.View.extend({
	tag: "<div>",
	container: null,
	baseCls: "game",
	panelTable: null,
	game: null,
	start: false,
	socket: null,
	counter: null,
	players: [],
	totalAnswers: null,
	actualQuestion: null,
	constructor : function(config) {
		var self = this;
		self._ensureElement();
		config = config || {};
		_.extend(self, config);
		self.$el = $(self.tag);
		self.$el.addClass(self.baseCls);
		self.initialize();
	},
	initialize: function(){
		var me = this;
		me.socket = io.connect('http://192.168.1.55:1337');
		me.showGame();
		me.render();
		me.socket.on('startGame'+me.game.id, function(content){
	    	me.startQuestion(content.question, content.turn);
	    	me.putPlayerInTurn(content.turn);
	    	me.checkPlayerAnswering(content.turn);
		});
		me.socket.on('checkanswer'+me.game.id, function(content){
			if(content.success == false)
			{
				if(content.noAnswer == false)
				{
					me.showMessageAlert("Incorrecto", "#D9534F");
				}
				setTimeout(function(){
					me.disableMessageAlert();
					me.disableAnswers(content.played);
					if(content.finish == false){
						me.startCounter(content.turn);
						if(content.question != null)
						{
							me.startQuestion(content.question, content.turn);
							me.putPlayerInTurn(content.turn);
						}
						me.checkPlayerAnswering(content.turn);
					}
				},1500);
			}
			else{
				me.showMessageAlert("Correcto", "#5CB85C");
				setTimeout(function(){
					if(content.question != null)
					{
						if(content.finish == false){
							me.startQuestion(content.question, content.turn);
							me.putPlayerInTurn(content.turn);
							me.checkPlayerAnswering(content.turn);
						}
						else{
							me.disableMessageAlert();
						}
					}	
				},1500);
			}
	    	me.changePoints(content.played, content.newPoints);
	    	if(content.finish == true){
	    		me.finishGame(content.rank);
	    	}
		});
		me.socket.on("stopcounter"+me.game.id, function(user){
			me.stopCounter();
		});
	},
	render: function(){
		this.container.append(this.$el);
	},

	startCounter: function(player)
	{
		var me = this;
		var user = qestionApp.session.getUser();
		$("#mainHeader").css("display", "none");
		$("#questionHeader").css("display", "block");
		me.counter = new Worker("js/QuestionCounter.js");
		var spanCount = $("#question-time-counter");
		var progressBar = $(".progress-bar", ".progress-header");
		me.counter.addEventListener('message', function(e) {
			var data = e.data;
			switch (data.cmd) {
		  		case "time":
		  			spanCount.html(data.value+"'");
		  			break;
		  		case "timeout":

		  			if(user.userId == player.player.id){
		  				me.checkQuestion("0", player);
		  			}
		  			me.showMessageAlert("Tiempo Agotado", "#F0AD4E");
		  			break;
		  		case "pbar":
		  			progressBar.css("width", data.value+"%");
		  			break;
		  		case "stop":
		  			me.counter.terminate();
		  			break;
		  	}
		}, false);
		me.counter.postMessage({'cmd': 'start', "totalSecond": "30", "percent": "300"});
	},

	startQuestion: function(question, player)
	{
		var me = this;
		me.paintQuestion(question);
		me.startCounter(player);
	},

	showGame: function()
	{
		var me = this;
		var user = qestionApp.session.getUser();
		var questionsDiv = $("<div></div>").addClass("div-numberquestion").attr("id", "numberquestion");
		var divImg = $("<div></div>").addClass('div-img-left');
		var img = $("<img/>").attr("src", qestionApp.server+"/image/"+user.photo+"?width=130&height=130").addClass("img-user");
		var text = "Pregunta "+me.game.askedQuestion+"/"+me.game.totalQuestions;
		var numberQuestion = $("<span></span>").html(text);
		questionsDiv.append(numberQuestion);
		var principalDiv = $("<div></div>").addClass("div-principal");
		var gameDiv = $("<div></div>").addClass("div-game").attr("id", "gamediv");
		var questionDiv = $("<div></div>").addClass("div-question").attr("id", "questiondiv");
		
		var text = $("<p></p>").html("Esperando Jugadores");
		var d = $("<div></div>").append(text);
		questionDiv.append(d);
		var divHeight = questionDiv.height()/2;
		var pHeight = d.height()/2
		var h = divHeight - pHeight
		text.css("padding-top", h+"px");
		var answersDiv = $("<div></div>").attr("id", "answersdiv");
		gameDiv.append(questionDiv).append(answersDiv);
		var playersPanel = $("<div></div>").addClass("panel panel-primary panel-players");
		var titlePanel = $("<div></div<").addClass("panel-heading").html("Jugadores");
		var bodyPanel = $("<table></table>").addClass("table");
		var thead = $("<thead></thead>");
		var trhead = $("<tr></tr>");
		var posTh = $("<th></th>").html("#");
		var playerTh = $("<th></th>").html("Jugador");
		var pointsTh = $("<th></th>").html("Puntos");
		var turnTh = $("<th></th>");
		var tbody = $("<tbody></tbody>");
		var playersDiv = $("<div></div>").addClass("list-group");
		$.each(me.game.players, function(key, value)
		{
			me.addPlayer(value, tbody);
		});
		if(me.start == true)
		{
			me.startGame();
		}
		me.socket.on('newPlayer'+me.game.id, function(content){
			me.addPlayer(content.player, tbody);
		});
		bodyPanel.append(thead.append(trhead.append(posTh).append(playerTh).append(pointsTh).append(turnTh))).append(tbody);
		playersPanel.append(titlePanel).append(bodyPanel);
		principalDiv.append(gameDiv).append(playersPanel);
		me.$el.append(divImg.append(img)).append(questionsDiv).append(principalDiv);
	},

	addPlayer: function(player, div)
	{
		var me = this;
		var divPlayer = $("<tr></tr>").attr("id", "tr"+player.player.id);
		var position = $("<th></th>").attr("scope", "row").html(player.position).css("vertical-align", "middle");
		var playerDiv = $('<div></div>');
		var img = $("<img/>").css("margin-left", "5px").attr("src", qestionApp.server+"/image/"+player.player.image+"?width=40&height=40").addClass("img-user-circular");
		var name = $("<span></span>").css("margin-left", "10px").html(player.player.name+" "+player.player.lastname);
		playerDiv.append(img).append(name);
		var tdPlayer = $("<td></td>").append(playerDiv);
		var tdPoints = $("<td></td>").attr("id", "points"+player.player.id).html(player.points).css({"text-align": "center","vertical-align": "middle"});
		var tdTurn = $("<td></td>").attr("id", "position"+player.player.id);
		// divPlayer.append(position).append(img).append(name);
		div.append(divPlayer.append(position).append(tdPlayer).append(tdPoints).append(tdTurn));
		me.players.push(player);
	},

	startGame: function()
	{
		var me = this;
		var header = qestionApp.getHeaders();
		$.ajax({
		  	type: "GET",
		  	headers: header,
			url: qestionApp.server+"/qestion/main/start/game/"+me.game.id,
			success: function(data){
				
			},
			error: function(xhr, status, error) {
		    	try{
			    	var obj = jQuery.parseJSON(xhr.responseText);
			    	var n = noty({
			    		text: obj.message,
			    		timeout: 1000,
			    		type: "error"
			    	});
		    	}catch(ex){
		    		var n = noty({
			    		text: "Error",
			    		timeout: 1000,
			    		type: "error"
			    	});
		    	}
	    	}
	    });
	},
	paintQuestion: function(question)
	{
		var me = this;
		me.game.askedQuestion = me.game.askedQuestion + 1;
		var questionsDiv = $("#numberquestion");
		questionsDiv.empty();
		var text = "Pregunta "+me.game.askedQuestion+"/"+me.game.totalQuestions;
		var numberQuestion = $("<span></span>").html(text);
		questionsDiv.append(numberQuestion);
		var numberQuestion = $("<span></span>").html(text);
		var questionDiv = $("#questiondiv");
		var answersDiv = $("#answersdiv");
		questionDiv.empty();
		answersDiv.empty();
		me.showMessage(questionDiv);
		var questionText = $("<p></p>").html(question.question);
		var d = $("<div></div>").append(questionText);
		questionDiv.append(d);
		var divHeight = questionDiv.height()/2;
		var pHeight = d.height()/2
		var h = divHeight - pHeight
		questionText.css("padding-top", h+"px");
		var totalQuestion = 1;
		$.each(question.answers, function(key, answer){
			var button = $("<div></div>").attr({
				"data-answer": answer['id'],
				"id": "btnanswer"+totalQuestion
			}).html(answer['answer']).addClass("btn btn-default btn-answer");
			button.attr("disabled", "disabled");
			answersDiv.append(button);
			totalQuestion++;
		})
		me.actualQuestion = question;
		me.totalAnswers = totalQuestion;
	},
	changePoints: function(player, points)
	{
		var td = $("#points"+player.id).html(points);
		var td = $("#position"+player.id).empty();
	},
	putPlayerInTurn: function(player)
	{
		var me = this;
		var tr = $("tr.player-turn");
		tr.removeClass("player-turn");
		var playerTr = $("#tr"+player.player.id);
		playerTr.addClass("player-turn");
	},

	checkQuestion: function(answerId, player)
	{
		var me = this;
		// me.counter.postMessage({'cmd': 'stop'});
		me.disableAnswers(player.player);
		var header = qestionApp.getHeaders();
		var data = {
			questionId: me.actualQuestion.id,
			answerId: answerId,
			gameId: me.game.id
		};
		$.ajax({
		  	type: "POST",
		  	headers: header,
			url: qestionApp.server+"/qestion/main/check/answer",
			data: JSON.stringify(data),
			contentType: 'application/json',
            dataType: "json",
			success: function(data){
			},
			error: function(xhr, status, error) {
		    	try{
			    	var obj = jQuery.parseJSON(xhr.responseText);
			    	var n = noty({
			    		text: obj.message,
			    		timeout: 1000,
			    		type: "error"
			    	});
		    	}catch(ex){
		    		var n = noty({
			    		text: "Error",
			    		timeout: 1000,
			    		type: "error"
			    	});
		    	}
	    	}
	    });
	},

	checkPlayerAnswering: function(player)
	{
		var me = this;
		var user = qestionApp.session.getUser();
		var td = $("#position"+player.player.id);
		var button = $("<div></div>").attr({
			"aria-label": "Left Align"
		}).css({
			"margin-top": "12px"
		});
		var span = $("<span></span>").addClass("glyphicon glyphicon-chevron-left").attr("aria-hidden", "true");
		td.append(button.append(span));
		if(user.userId == player.player.id){
			console.log("entre a activar botones");
			for(var i = 1; i <= me.totalAnswers; i++)
			{
				var btn = $("#btnanswer"+i).removeAttr("disabled");
				btn.click(function(){
					var answerId = $(this).attr("data-answer");
					me.checkQuestion(answerId, player);
					var data = {
						"message": "stopcounter"+me.game.id,
						"content": user
					};
					me.socket.emit("broadcast", data);
				})
			}
		}
	},
	stopCounter: function(){
		var me = this;
		me.counter.postMessage({'cmd': 'stop'});
	},
	disableAnswers: function(player){
		var me = this;
		var user = qestionApp.session.getUser();
		if(user.userId == player.id){
			for(var i = 1; i <= me.totalAnswers; i++)
			{
				var btn = $("#btnanswer"+i).attr("disabled", "disabled");
			}
		}
	},
	showMessageAlert: function(message, color){
		var div = $("#dataalert").css("display", "");
		
		$("#spanmsg").html(message).css("color", color);
	},
	disableMessageAlert: function()
	{
		$("#spanmsg").html("");
		$("#dataalert").css("display", "none");
	},
	showMessage: function(container){
		var height = container.height();
		var width = container.width();

		var divAlert = $('<div></div>').attr("id", "dataalert")
		 	.css("position", "absolute")
		 	.css("width", width)
		 	.css("height", height)
		 	.css("display", "none")
		 	.css("margin-top", "40px")
		 	.css("z-index", "99");
		 var msg = $("<span></span>").attr("id", "spanmsg").addClass("alert-msg");
		 divAlert.append(msg);
		 container.append(divAlert);
	},
	finishGame: function(players)
	{
		$.each(players, function(key, value){
			console.log("rank");
			console.log(key);
			console.log(value);
		})
		
		var me = this;
		var divButtons = $("<div></div>").addClass("div-iniciar");
		var buttonExit = $("<button></button>").html("Salir").addClass("btn btn-danger btn-iniciar").attr("data-id", "btnsalir");
		divButtons.append(buttonExit);
		
		var img = $("<img />").attr("src", "images/logo.png");
		var message = $('<h3></h3>').html("Juego Finalizado").addClass('message-final-duel');
		var messageDiv = $('<div></div>').append(message).css('text-align', 'center');
		divButtons.css('text-align', 'center');
		var divFirst = $("<div></div>").addClass("winner");
		var firstWinText = $('<h3></h3>').html("Ganador").addClass('message-final-duel');
		var divPlayerWin = $("<div></div>").addClass("divwinner");
		var imgWin = $("<img/>").css("margin-left", "5px").attr("src", qestionApp.server+"/image/"+players[0].player.image+"?width=150&height=150").addClass("img-user-circular");
		var nameWin = $("<span></span>").html(players[0].player.name+" "+players[0].player.lastname);
		var pointsWin = $("<span></span>").html(players[0].points);
		divPlayerWin.append(imgWin).append(nameWin).append(pointsWin);
		divFirst.append(firstWinText).append(divPlayerWin)

		var others = $("<div></div>").addClass("other-player");
		var second = $("<div></div>").addClass("second-place");
		var imgSecond = $("<img/>").css("margin-left", "5px").attr("src", qestionApp.server+"/image/"+players[1].player.image+"?width=50&height=50").addClass("img-user-circular");
		var nameSecond = $("<span></span>").html(players[1].player.name+" "+players[1].player.lastname);
		var pointsSecond = $("<span></span>").html(players[1].points).css("margin-left", "10px");
		second.append(imgSecond).append(nameSecond).append(pointsSecond);

		var third = $("<div></div>").addClass("third-place");
		var imgThird = $("<img/>").css("margin-left", "5px").attr("src", qestionApp.server+"/image/"+players[2].player.image+"?width=50&height=50").addClass("img-user-circular");
		var nameThird = $("<span></span>").html(players[2].player.name+" "+players[2].player.lastname);
		var pointsThird = $("<span></span>").html(players[2].points).css("margin-left", "10px");
		third.append(imgThird).append(nameThird).append(pointsThird)

		var fourth = $("<div></div>").addClass("fourth-place");
		var imgFourth = $("<img/>").css("margin-left", "5px").attr("src", qestionApp.server+"/image/"+players[3].player.image+"?width=50&height=50").addClass("img-user-circular");
		var nameFourth = $("<span></span>").html(players[3].player.name+" "+players[3].player.lastname);
		var pointsFourth = $("<span></span>").html(players[3].points).css("margin-left", "10px");
		fourth.append(imgFourth).append(nameFourth).append(pointsFourth);
		others.append(second).append(third).append(fourth);
		var principalDiv = $("<div></div>").addClass("question-notify").append(img).append(messageDiv).append(divFirst).append(others).append(divButtons);
		
		// me.$el.append(img).append(divButtons);
		
		var modal = $("<div></div>")
			.css("position", "absolute")
		 	.css("width", "100%")
		 	.css("height", "100%")
		 	.css("opacity", "0.7")
		 	.css("background-color", "#888")
		 	.css("z-index", "1");
		var divAns = $('<div></div>')
		 	.css("position", "absolute")
		 	.css("top", "0px")
		 	.css("width", "100%")
		 	.css("height", "100%")
		 	// .css("background-color", "#888")
		 	.css("display", "none")
		 	.css("z-index", "100")
		 	.append(modal)
		 	.append(principalDiv);

		$(document.body).append(divAns);
    	// divAns.css("display", "block");
    	divAns.fadeIn(500);
		buttonExit.click(function(e){
    		divAns.fadeOut(500, function(){
    			Backbone.history.navigate("#home", true);
    			divAns.remove();
    		})
		})
	}
});
